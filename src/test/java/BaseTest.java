import config.Config;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
    protected Config config;
    protected String baseUrl;

    @BeforeMethod
    public void baseBeforeMethod() {
        config = new Config();
        baseUrl = config.getApplicationUrl();
    }
}