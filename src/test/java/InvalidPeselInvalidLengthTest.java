import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utils.ValidationErrorRepository;

import static io.restassured.RestAssured.get;

public class InvalidPeselInvalidLengthTest extends BaseTest {

    @DataProvider
    public Object[][] getData_InvalidLength() {
        return new Object[][]{
                {"5703064399"},             // too short (BVA)
                {"570306439988"},           // too long (BVA)
                {"1"},                      // too short (EP)
                {"12345678901234567890"}    // too long (EP)
        };
    }

    @Test(dataProvider = "getData_InvalidLength")
    public void testGetRequest_InvalidLength(String expectedPesel) {
        Response response = get(baseUrl + expectedPesel);

        String actualPesel = response.path("pesel");
        Boolean actualIsValid = response.path("isValid");
        String actualBirthDate = response.path("birthDate");
        String actualSex = response.path("sex");
        String actualErrorCode = response.path("errors[0].errorCode");
        String actualErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(actualPesel, expectedPesel);
        Assert.assertFalse(actualIsValid);
        Assert.assertNull(actualBirthDate);
        Assert.assertNull(actualSex);
        Assert.assertEquals(actualErrorCode, ValidationErrorRepository.InvalidLength.code);
        Assert.assertEquals(actualErrorMessage, ValidationErrorRepository.InvalidLength.description);
    }
}
