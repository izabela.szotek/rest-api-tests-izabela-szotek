import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utils.ValidationErrorRepository;

import static io.restassured.RestAssured.get;

public class InvalidPeselInvalidCheckSumTest extends BaseTest {

    @DataProvider
    public Object[][] getData_InvalidCheckSum() {
        return new Object[][]{
                // all possible invalid values (for the same date)
                {"57030643990", "1957-03-06T00:00:00", "Male"},
                {"57030643991", "1957-03-06T00:00:00", "Male"},
                {"57030643992", "1957-03-06T00:00:00", "Male"},
                {"57030643993", "1957-03-06T00:00:00", "Male"},
                {"57030643994", "1957-03-06T00:00:00", "Male"},
                {"57030643995", "1957-03-06T00:00:00", "Male"},
                {"57030643996", "1957-03-06T00:00:00", "Male"},
                {"57030643997", "1957-03-06T00:00:00", "Male"},
                {"57030643999", "1957-03-06T00:00:00", "Male"},
        };
    }

    @Test(dataProvider = "getData_InvalidCheckSum")
    public void testGetRequest_InvalidCheckSum(String expectedPesel, String expectedBirthDate, String expectedSex) {
        Response response = get(baseUrl + expectedPesel);

        String actualPesel = response.path("pesel");
        Boolean actualIsValid = response.path("isValid");
        String actualBirthDate = response.path("birthDate");
        String actualSex = response.path("sex");
        String actualErrorCode = response.path("errors[0].errorCode");
        String actualErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(actualPesel, expectedPesel);
        Assert.assertFalse(actualIsValid);
        Assert.assertEquals(actualBirthDate, expectedBirthDate);
        Assert.assertEquals(actualSex, expectedSex);
        Assert.assertEquals(actualErrorCode, ValidationErrorRepository.InvalidCheckSum.code);
        Assert.assertEquals(actualErrorMessage, ValidationErrorRepository.InvalidCheckSum.description);
    }
}
