import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class ValidPeselTest extends BaseTest {

    @DataProvider
    public Object[][] getData_ValidPesel() {
        return new Object[][]{
                {"57030643998", "1957-03-06T00:00:00", "Male", "valid length (BVA, EP), valid characters"},          //
                // valid day (boundry values)
                {"80013185587", "1980-01-31T00:00:00", "Female", "January 31st"},
                {"97022891462", "1997-02-28T00:00:00", "Female", "February 28th"},
                {"00222925918", "2000-02-29T00:00:00", "Male", "February 29th (gap year)"},
                {"12033132728", "1912-03-31T00:00:00", "Female", "March 31st"},
                {"92243081618", "2092-04-30T00:00:00", "Male", "April 30th"},
                {"60053146827", "1960-05-31T00:00:00", "Female", "May 31st"},
                {"45063096793", "1945-06-30T00:00:00", "Male", "June 30th"},
                {"37073178426", "1937-07-31T00:00:00", "Female", "July 31st"},
                {"00083163645", "1900-08-31T00:00:00", "Female", "August 31st"},
                {"10293028159", "2010-09-30T00:00:00", "Male", "September 30th"},
                {"03303133747", "2003-10-31T00:00:00", "Female", "October 31st"},
                {"61313054847", "2061-11-30T00:00:00", "Female", "November 30th"},
                {"38123151473", "1938-12-31T00:00:00", "Male", "December 31st"},
                // valid month (boundry values)
                {"59012328675", "1959-01-23T00:00:00", "Male", "month: 01"},
                {"41121556423", "1941-12-15T00:00:00", "Female", "month: 12"},
                {"86210434682", "2086-01-04T00:00:00", "Female", "month: 21"},
                {"59321816249", "2059-12-18T00:00:00", "Female", "month: 32"},
                {"27410841986", "2127-01-08T00:00:00", "Female", "month: 41"},
                {"07521859937", "2107-12-18T00:00:00", "Male", "month: 52"},
                {"82611013208", "2282-01-10T00:00:00", "Female", "month: 61"},
                {"00721018218", "2200-12-10T00:00:00", "Male", "month: 72"},
                {"06810628218", "1806-01-06T00:00:00", "Male", "month: 81"},
                {"96922611051", "1896-12-26T00:00:00", "Male", "month: 92"}
        };
    }

    @Test(dataProvider = "getData_ValidPesel")
    public void test_ValidPesel(String expectedPesel, String expectedBirthDate, String expectedSex, String description) {

        Response response = get(baseUrl + expectedPesel);

        String actualPesel = response.path("pesel");
        Boolean actualIsValid = response.path("isValid");
        String actualBirthDate = response.path("birthDate");
        String actualSex = response.path("sex");

        Assert.assertEquals(actualPesel, expectedPesel);
        Assert.assertTrue(actualIsValid);
        Assert.assertEquals(actualBirthDate, expectedBirthDate);
        Assert.assertEquals(actualSex, expectedSex);

        System.out.println("Test Pesel returns valid pesel for: " + description);
    }

}
