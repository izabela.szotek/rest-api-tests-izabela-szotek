import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.ValidationErrorRepository;

import static io.restassured.RestAssured.get;

public class InvalidPeselInvalidCharactersTest extends BaseTest {

    @Test
    public void testGetRequest_InvalidCharacters() {
        String expectedPesel = "5703a643998";

        Response response = get(baseUrl + expectedPesel);

        String actualPesel = response.path("pesel");
        Boolean actualIsValid = response.path("isValid");
        String actualBirthDate = response.path("birthDate");
        String actualSex = response.path("sex");
        String actualErrorCode = response.path("errors[0].errorCode");
        String actualErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(actualPesel, expectedPesel);
        Assert.assertFalse(actualIsValid);
        Assert.assertNull(actualBirthDate);
        Assert.assertNull(actualSex);
        Assert.assertEquals(actualErrorCode, ValidationErrorRepository.InvalidCharacters.code);
        Assert.assertEquals(actualErrorMessage, ValidationErrorRepository.InvalidCharacters.description);
    }
}
