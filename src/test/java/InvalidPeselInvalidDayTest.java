import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utils.ValidationErrorRepository;

import static io.restassured.RestAssured.get;

public class InvalidPeselInvalidDayTest extends BaseTest{

    @DataProvider
    public Object[][] getData_InvalidDay() {
        return new Object[][]{
                // Invalid day - Boundry Values
                {"57010043987", "Female", "January 0th"},
                {"57019943981", "Female", "January 99th"},
                {"57013243995", "Male", "January 32nd"},
                {"57023043992", "Male", "February 30th"},
                {"57022943989", "Female", "February 29th, not a gap year"},
                {"57033243997", "Male", "March 32nd"},
                {"57043143991", "Male", "April 31st"},
                {"57053243999", "Male", "May 32nd"},
                {"57063143993", "Male", "June 31st"},
                {"57073243991", "Male", "July 32nd"},
                {"57083243992", "Male", "August 32nd"},
                {"57093143989", "Female", "September 31st"},
                {"57103243980", "Female", "October 32nd"},
                {"57113143984", "Female", "November 31st"},
                {"57123243982", "Female", "December 32nd"}
        };
    }

    @Test(dataProvider = "getData_InvalidDay")
    public void testGetRequest_InvalidDay(String expectedPesel, String expectedSex, String description) {
        Response response = get(baseUrl + expectedPesel);

        String actualPesel = response.path("pesel");
        Boolean actualIsValid = response.path("isValid");
        String actualBirthDate = response.path("birthDate");
        String actualSex = response.path("sex");
        String actualErrorCode = response.path("errors[0].errorCode");
        String actualErrorMessage = response.path("errors[0].errorMessage");

        Assert.assertEquals(actualPesel, expectedPesel);
        Assert.assertFalse(actualIsValid);
        Assert.assertNull(actualBirthDate);
        Assert.assertEquals(actualSex, expectedSex);
        Assert.assertEquals(actualErrorCode, ValidationErrorRepository.InvalidDay.code);
        Assert.assertEquals(actualErrorMessage, ValidationErrorRepository.InvalidDay.description);

        System.out.println("Test Pesel returns invalid day error due to: " + description);
    }

}
