import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.get;

public class ResponseStatusTest extends BaseTest {

    @DataProvider
    public Object[][] getData_ResponseCode() {
        return new Object[][]{
                {"57030643998", 200},
                {"", 400}
        };
    }

    @Test(dataProvider = "getData_ResponseCode")
    public void testGetRequest_ResponseStatusCodeOk(String testedPesel, int expectedStatusCode) {
        Response response = get(baseUrl + testedPesel);

        Assert.assertEquals(response.statusCode(), expectedStatusCode);
    }
}
