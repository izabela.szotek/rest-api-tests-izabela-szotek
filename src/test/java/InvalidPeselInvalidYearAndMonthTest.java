import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import utils.ValidationErrorRepository;

import static io.restassured.RestAssured.get;

public class InvalidPeselInvalidYearAndMonthTest extends BaseTest {

    @DataProvider
    public Object[][] getData_InvalidYearAndMonth() {
        return new Object[][]{
                {"57000243980", "Female", "month: 00"},
                {"57130243986", "Female", "month: 13"},
                {"57200243986", "Female", "month: 20"},
                {"57330243982", "Female", "month: 33"},
                {"57400243982", "Female", "month: 40"},
                {"57530243988", "Female", "month: 53"},
                {"57600243988", "Female", "month: 60"},
                {"57730243984", "Female", "month: 73"},
                {"57800243984", "Female", "month: 80"},
                {"57930243980", "Female", "month: 93"},
                {"57990243986", "Female", "month: 99"}
        };
    }

    @Test(dataProvider = "getData_InvalidYearAndMonth")
    public void testGetRequest_InvalidYearAndMonth(String expectedPesel, String expectedSex, String description) {
        Response response = get(baseUrl + expectedPesel);

        String actualPesel = response.path("pesel");
        Boolean actualIsValid = response.path("isValid");
        String actualBirthDate = response.path("birthDate");
        String actualSex = response.path("sex");
        String actualErrorCode_1 = response.path("errors[0].errorCode");
        String actualErrorMessage_1 = response.path("errors[0].errorMessage");
        String actualErrorCode_2 = response.path("errors[1].errorCode");
        String actualErrorMessage_2 = response.path("errors[1].errorMessage");

        Assert.assertEquals(actualPesel, expectedPesel);
        Assert.assertFalse(actualIsValid);
        Assert.assertNull(actualBirthDate);
        Assert.assertEquals(actualSex, expectedSex);
        Assert.assertEquals(actualErrorCode_1, ValidationErrorRepository.InvalidYear.code);
        Assert.assertEquals(actualErrorMessage_1, ValidationErrorRepository.InvalidYear.description);
        Assert.assertEquals(actualErrorCode_2, ValidationErrorRepository.InvalidMonth.code);
        Assert.assertEquals(actualErrorMessage_2, ValidationErrorRepository.InvalidMonth.description);

        System.out.println("Test Pesel returns invalid month and year error due to " + description);
    }
}
