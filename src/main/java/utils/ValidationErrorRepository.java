package utils;

public class ValidationErrorRepository {
    public static ValidationError InvalidLength = new ValidationError("INVL", "Invalid length. Pesel should have exactly 11 digits.");
    public static ValidationError InvalidCharacters = new ValidationError("NBRQ", "Invalid characters. Pesel should be a number.");
    public static ValidationError InvalidYear = new ValidationError("INVY", "Invalid year.");
    public static ValidationError InvalidMonth = new ValidationError("INVM", "Invalid month.");
    public static ValidationError InvalidDay = new ValidationError("INVD", "Invalid day.");
    public static ValidationError InvalidCheckSum = new ValidationError("INVC", "Check sum is invalid. Check last digit.");
}