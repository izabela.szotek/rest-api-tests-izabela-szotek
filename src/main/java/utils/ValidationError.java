package utils;

public class ValidationError {
    public String code;
    public String description;

    public ValidationError(String code, String description)
    {
        this.code = code;
        this.description = description;
    }
}